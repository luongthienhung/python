import cv2
import glob
import numpy as np
import sys 
from sklearn.model_selection import train_test_split

from sklearn.preprocessing import StandardScaler
#thuật toán phân chia dữ liệu
from sklearn.svm import SVC
# đánh giá chất lượng
from sklearn.metrics import accuracy_score

# Đường dẫn đến tệp video và hình ảnh
path_cars ='clipcar/*.mp4'
#path_cars = cv2.VideoCapture('Xe.mp4')
path_non_cars = 'people/*.mp4'

# Đọc ảnh và gán nhãn (1 cho xe, 0 cho không phải xe)
images = [1]
labels = [0]

# Đọc ảnh của xe
#gob.glob là thư viện đọc file của python
for file_path in glob.glob(path_cars):
    img = cv2.imread(file_path)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Thay đổi kích thước hình ảnh thành kích thước cố định (ví dụ: 64x64)
    img_gray_resized = cv2.resize(img_gray, (64, 64))

    # kiểm tra xem hình ảnh có đúng hình dạng không
    if img_gray_resized.shape == (64, 64):
        images.append(img_gray_resized,1)
        labels.append(0)
    else:
        print(f"Ignoring image with incorrect shape: {file_path}")
# Đọc ảnh không phải xe
#gob.glob là thư viện đọc file của python
for file_path in glob.glob(path_non_cars):
    img = cv2.imread(file_path)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Thay đổi kích thước hình ảnh thành kích thước cố định (ví dụ: 64x64)
    img_gray_resized = cv2.resize(img_gray, (64, 64))
    
    # kiểm tra xem hình ảnh có đúng hình dạng không
    if img_gray_resized.shape == (64, 64):
        images.append(img_gray_resized,0)
        labels.append(0)
    else:
        print(f"Ignoring image with incorrect shape: {file_path}")
    

# Chuyển đổi danh sách hình ảnh thành mảng NumPy
images = np.array(images)
labels = np.array(labels)
