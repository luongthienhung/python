import cv2
import numpy as np
from xml.etree import ElementTree as ET

cap = cv2.VideoCapture('clipcar.mp4')
with open("/Users/hung/tranning_cars.xml","r") as f:
    tree = ET.parse(f)
root = tree.getroot()




# lặp qua cấc phần tử
for child in root:
    print(child.tag)
    print(child.attrib)



# Tạo một vùng chứa vạch kẻ
w, h = 300, 300
roi = (300, 50, w, h)

# Biến đếm
count  = 0

# Lặp lại cho đến khi kết thúc video
while  cap.isOpened():
    # Đọc khung hình từ video
    ret, frame = cap.read()
    input_frame = frame
    #image_np_expanded = np.expand_dims(input_frame, axis=0)
    




    # Nếu khung hình là không hợp lệ, thoát khỏi vòng lặp
    if not ret:
        break

    # Chuyển đổi khung hình sang ảnh thang độ xám
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Xác định các contours trong ảnh thang độ xám
    contours, _  = cv2.findContours(gray_frame, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    

    #Nếu một cái xe qua vạch kẻ, hãy tăng biến đếm
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        if 50 < h < 200 and 50 < w < 200: # Giả sử xe có kích thước tối thiểu
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 10)
            count += 1
            
        
        
    

    # Hiển thị số xe đã đi qua vạch kẻ
    #cv2.putText(frame, f"so xe: {count}", (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
    cv2.putText(frame, f"so xe: {count}", (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
    # Hiển thị khung hình
    cv2.imshow("Video", frame)
    
    

    # Dừng vòng lặp nếu người dùng nhấn phím ESC
    key = cv2.waitKey(1) & 0xFF
    if key == 27:
        break


# Đóng đối tượng VideoCapture
cap.release()
cv2.destroyAllWindows()




